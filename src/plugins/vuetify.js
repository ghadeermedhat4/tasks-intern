import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: "#DE1D9D",
        secondary: "#212CFF",
        accent: "#8C9EFF",
        error: "#B71C1C",
        gray: "#F7F8F9",
        lightblue:"#C5CAE9",
        verylightblue:"#E8EAF6",
        pinklighten4:"#F8BBD0",
        pinklighten5 :"#FCE4EC",
        greylighten2:"#EEEEEE",
        greylighten1:"#BDBDBD",
        greydarken1:"#757575",
        true:"#00C853",
        wrong:"#DD2C00"
      },
    },
  },
});
