// i18n.js
import Vue from 'vue';
import VueI18n from 'vue-i18n';
import en from './locales/en.json';
import ar from './locales/ar.json';

Vue.use(VueI18n);

const i18n = new VueI18n({
  locale: localStorage.getItem('locale') || 'en',
  messages: {
    en,
    ar
  }
});

export default i18n;
