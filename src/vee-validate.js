import { extend, localize } from 'vee-validate';
import * as rules from 'vee-validate/dist/rules';

Object.keys(rules).forEach(rule => {
  extend(rule, rules[rule]);
});


localize({
 
  en: {
    messages: {
      required: 'This field is required.',
      
    }
  }
});
